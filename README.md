# STM32Python Presentations

Website : https://stm32python.gitlab.io/presentations

## For development

```bash
 npm install browser-sync gulp --save-dev
 gulp
```

> Remark: remark-latest.min.js can be stored locally for offline developement
```bash
wget https://remarkjs.com/downloads/remark-latest.min.js
```

## For deployement

Navigate to your project’s CI/CD > Pipelines and click Run pipeline to trigger GitLab CI/CD to build and deploy your site to the server. The website will be updated 30 minutes after the pipeline completion.


## TODOLIST
* installation de remark via npm install