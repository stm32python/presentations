
title: STM32 Python
subtitle: Kit pédagogique pour l'Internet des Objets
class: center, middle

# {{ title }}
## {{ subtitle }}

<!-- Trouvez un logo -->

---

# Agenda

1. Contexte
2. Objectif
3. Partenaires
4. Comment démarrer ?
5. Comment contribuer ?

---
# Contexte

---
# Objectif
Le projet STM32 Python développe des kits pédagogiques pour l'initiation à l'Internet des Objets en s'appuyant sur le microcontrôleur STM32 et le langage Python.
---
# Partenaires

<!-- Mettre les logos -->
* Rectorats des Academies de Grenoble et d'Aix-Marseille
* ST Microelectronics
* Inventhys
* Univ. Grenoble Alpes / Polytech Grenoble

---
<!---
background-image: url(stm_iotkit.jpg)
background-position: center;
background-repeat: no-repeat;
background-size: contain;      /* applied using JavaScript only if background-image is larger than slide */
--->

# Comment démarrer ?


<div align="right">
    <img src="stm_iotkit.jpg" width="400"/>
</div>

* Acheter un des [kits préconisés](http://stm32python.gitlab.io/doc/stm32/kit/nucleo/python/2019/11/04/kit.html)
* Suivre les tutoriels sur https://gitlab.com/stm32python/doc/

---
# Installation

```python
s = "STM32 Python"
print s
```

```terminal
me@myhost ~/gitlab/stm32python/hello $ start
...

```

---
# Comment contribuer ?

Enseignant, Etudiant, Lycéen, Ingénieur, Hobbyiste, vous avez réalisé un complément à ce tutoriel : n'hésitez pas nous contacter pour contribuer au projet !

https://gitlab.com/stm32python/doc/contributeurs.md