
name: title
title: Annexe Test Remark
subtitle: https://github.com/gnab/remark/wiki/Markdown
class: center, middle

# {{ title }}
## {{ subtitle }}

---
name: agenda

# Test  Remark

```ruby
def add(a,b)
*  a + b
end

# Notice how there is no return statement.
```

.footnote[.red.bold[*] Important footnote]

---
# Slide


HTML

<!---
I'm a comment.
--->

Empty Link

[//]: # (I'm a comment)

[the title](#title)

[the agenda](#agenda)


???
Some note.
---

# Agenda

1. Contexte
2. Objectif
3. Partenaires
4. Comment démarrer ?
5. Comment contribuer ?

---
background-image: url(stm_iotkit.jpg)
background-position: center;
background-repeat: no-repeat;
background-size: contain;      /* applied using JavaScript only if background-image is larger than slide */

# Contexte

---

layout: true

# Section

---

## Sub section 1

Foo

---

## Sub section 2

Bar

---

layout: false

# Agenda

--
1. Introduction

--
2. Markdown formatting

Le projet STM32 Python développe des kits pédagogiques pour l'initiation à l'Internet des Objets en s'appuyant sur le microcontrôleur STM32 et le langage Python.

---
# Partenaires

* Rectorat de l'Academie de Grenoble

--

* ST Microelectronics

--

* Inventhys

--

* Univ. Grenoble Alpes

---
# Comment démarrer ?

https://gitlab.com/stm32python


[STEVAL-STLKT01V1](https://www.st.com/content/st_com/en/products/evaluation-tools/solution-evaluation-tools/sensor-solution-eval-boards/steval-stlkt01v1.html)

---
# Comment contribuer ?

https://gitlab.com/stm32python

